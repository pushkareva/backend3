<?php


header('Content-Type: text/html; charset=UTF-8');


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
 
  if (!empty($_GET['save'])) {
   
    print('Спасибо, результаты сохранены.');
  }
  
  include('form.php');
  
  exit();
}

$errors = FALSE;

if (empty($_POST['fio'])) {
    print('Заполните имя.<br>');
    $errors = TRUE;
}
else if (!preg_match("/^[а-яА-Я ]+$/u", $_POST['fio'])) {
    print('Недопустимые символы в имени.<br>');
    $errors = TRUE;
}

//email
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    print('Проверьте правильность ввода email<br>');
    $errors = TRUE;
}

//year
if (empty($_POST['year'])) {
    print('Заполните год.<br>');
    $errors = TRUE;
}
else {
    $year = $_POST['year'];
    if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) < 2020)) {
        print("Укажите корректный год.<br>");
        $errors = TRUE;
    }
}



//abilities
$ability_data = ['immort', 'wall', 'levit', 'invis'];
if (empty($_POST['abilities'])) {
    print('Выберите способность<br>');
    $errors = TRUE;
}
else {
    $abilities = $_POST['abilities'];
    foreach ($abilities as $ability) {
        if (!in_array($ability, $ability_data)) {
            print('Недопустимая способность<br>');
            $errors = TRUE;
        }
    }
}
$ability_insert = [];
foreach ($ability_data as $ability) {
    $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}

//accept
if (empty($_POST['accept'])) {
    print("Вы не приняли соглашение!<br>");
    $errors = TRUE;
}



if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u24902';
$pass = '3346673';
$db = new PDO('mysql:host=localhost;dbname=u24902', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, sex = ?, limbs = ?, immort = ?, wall = ?, levit = ?, invis = ?, text = ?, accept = ?");
  $stmt -> execute([$_POST['fio'], $_POST['email'], intval($year), intval($_POST['sex']), intval($_POST['limbs']), $ability_insert['immort'], $ability_insert['wall'], $ability_insert['levit'], $ability_insert['invis'], $_POST['text'], intval($_POST['accept'])]);
}
catch(PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}



// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1'); 
